const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      resa
        .status(200)
        .json({
          message:
            "Aqui estão os usuários captados da Api pública JSONPlaceholder ",
          users,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }

  static async getUsersWebSiteIO(req, res) {
    try{
      const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".io")
      )
      const banana = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .io",
          users, banana
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }
  }

  static async getUsersWebSiteCOM(req, res) {
    try{
      const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".com")
      )
      const usuarios = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .com",
          users, usuarios
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }
  }

  static async getUsersWebSiteNET(req, res) {
    const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      const users = response.data.filter( 
        (user) => user.website.endsWith(".net")
      )
      const usuarios = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com domínio .net",
          users, usuarios
        });
    }
    catch(error){
      res.status(500).json({ error: "Deu ruim" });
    }

    static async getCountDomain(req, res) {
      try {
        const { dominio } = req.query;
    
        if (!dominio) {
          return res.status(400).json({ error: "Parâmetro 'dominio' não fornecido" });
        }
    
        const response = await axios.get("https://jsonplaceholder.typicode.com/users");
        const users = response.data.filter(user => user.website.endsWith("." + dominio));
    
        const count = users.length;
    
        res.status(200).json({
          message: `Número de usuários com domínio .${dominio}: ${count}`,
          users
        });
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Ocorreu um erro ao processar a solicitação" });
      }
    }
  
};
