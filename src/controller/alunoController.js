module.exports = class alunoController {
  static async postAluno(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;

    console.log({
      nome: nome,
      idade: idade,
      profissao: profissao,
      cursoMatriculado: cursoMatriculado,
    });
    console.log(updateStatus);
    res.status(200).json({ message: "Aluno editado", type: "success" });
  }

  static async updateAluno(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    if (
      nome !== "" &&
      idade >= 1 &&
      profissao !== "" &&
      cursoMatriculado !== ""
    ) {
      return res.status(200).json({ mensage: "Aluno editado com sucesso!" });
    } else {
      return res.status(400).json({ message: "Espaço em Branco!" });
    }
  }
  
  static async deleteAlunos(req, res) {
    if (req.params.id >= 1) {
    return res.status(200).json({ Message: "Aluno removido!" })
}else {
      return res.status(400).json({ message: "Aluno não removido!" });
    }
  }

};
